import React from "react";

export const hourTime = () => {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    return h + ":" + (m < 10 ? ("0" + m) : m);
}

export const getDate = () => {
    let  today 		= new Date();
    let  dd 		= String(today.getDate()).padStart(2, '0');
    let  mm 		= String(today.getMonth() + 1).padStart(2, '0'); //janvier = 0
    let  yyyy 		= today.getFullYear();
    return `${yyyy}-${mm}-${dd}`; 
}