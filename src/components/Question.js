import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { GetAllQuestions } from "../store/actions/questions";
import {useSelector, useStore} from "react-redux";
import { Link, useParams, useNavigate } from 'react-router-dom';
import Header from '../components/Header';


export function Question() {
    const store = useStore();
    const { id } = useParams();
    const navigate = useNavigate();
    const listQuestions = useSelector(state => state.questions.list);
    const [loading, setLoading] = React.useState(false);
    const _GetAllQuestions = args => GetAllQuestions(args)(store.dispatch, store.getState);
    useEffect(() => {
        _GetAllQuestions().then(() => {
            setLoading(true);
        });
    }, []);
    return (
        <div className="App bg-[#F7F8F9]">
            <Header loading={loading} setLoading={setLoading} />
            <div className="py-8 px-14">
                {loading && listQuestions.map((item) => {
                    return (
                        <div className="question__box bg-[#F9F9F9] rounded-lg overflow-hidden mb-5" key={`questions_${item.id}`}>
                            <div className="question__box--title flex items-center rounded-lg h-12 text-base font-bold justify-between px-4 py-2 bg-white bg-opacity-70">
                                <div className="rightside flex items-center">
                                    <img src="https://dummyimage.com/32x32/000/fff" alt="" className="w-8 h-w-8 rounded-lg ml-3 " />
                                    <h3>{item.title}</h3>
                                </div>
                                <div className="leftside flex items-center">
                                    <div className="pl-3 ml-3 border-l">
                                        <div className="text-[#222222] font-bold text-xs">
                                            <span className="text-[#777777] font-normal text-xs ml-[3px]">تماس :</span> 
                                            {item.created_on.hour}
                                        </div>
                                    </div>
                                    <div className="pl-3 ml-3 border-l">
                                        <div className="text-[#222222] font-bold text-xs">
                                            <span className="text-[#777777] font-normal text-xs ml-[3px]">تاریخ :</span>
                                            {item.created_on.date}
                                        </div>
                                    </div>
                                    <div className="">
                                        <div className="text-[#222222] font-bold text-xs flex items-center">
                                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" clipRule="evenodd" d="M9.75 3C6.02208 3 3 6.02208 3 9.75C3 10.4379 3.14554 11.1468 3.35732 11.8091L3.36068 11.8196C3.57868 12.5014 3.74647 13.0261 3.86295 13.4222C3.97485 13.8028 4.05981 14.1293 4.08134 14.3849C4.10678 14.6867 4.12001 14.9487 4.08147 15.2236C4.04294 15.4985 3.9582 15.7467 3.85076 16.0299C3.79524 16.1763 3.71923 16.3302 3.6228 16.5H9.75C13.4779 16.5 16.5 13.4779 16.5 9.75C16.5 6.02208 13.4779 3 9.75 3ZM1.63165 16.8254C1.54861 16.9462 1.5 17.0924 1.5 17.25C1.5 17.6642 1.83579 18 2.25 18H9.75C14.3063 18 18 14.3063 18 9.75C18 5.19365 14.3063 1.5 9.75 1.5C5.19365 1.5 1.5 5.19365 1.5 9.75C1.5 10.6394 1.6862 11.5079 1.92858 12.2659C2.15063 12.9604 2.31298 13.4683 2.42388 13.8454C2.53994 14.2401 2.57966 14.4281 2.58664 14.5109C2.61071 14.7964 2.60979 14.9169 2.59599 15.0154C2.58219 15.1138 2.54994 15.2299 2.44829 15.4979C2.3664 15.7137 2.17416 16.0116 1.63165 16.8254Z" fill="#9CAEBB"/>
                                                <path fillRule="evenodd" clipRule="evenodd" d="M5.66675 8.08334C5.66675 7.66913 6.00253 7.33334 6.41675 7.33334H13.0834C13.4976 7.33334 13.8334 7.66913 13.8334 8.08334C13.8334 8.49756 13.4976 8.83334 13.0834 8.83334H6.41675C6.00253 8.83334 5.66675 8.49756 5.66675 8.08334Z" fill="#9CAEBB"/>
                                                <path fillRule="evenodd" clipRule="evenodd" d="M5.66675 11.4167C5.66675 11.0024 6.00253 10.6667 6.41675 10.6667H9.75008C10.1643 10.6667 10.5001 11.0024 10.5001 11.4167C10.5001 11.8309 10.1643 12.1667 9.75008 12.1667H6.41675C6.00253 12.1667 5.66675 11.8309 5.66675 11.4167Z" fill="#9CAEBB"/>
                                            </svg>
                                            <span className="text-[#777777] font-normal text-xs mr-1">
                                                {item.answers.length}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="question__box--answer p-5 text-[#222222] text-sm font-normal">
                                <p>{item.question}</p>
                                <div className="text-left">
                                    <button onClick={() => navigate(`/questions/${item.id}`, { state: true })} className="p-2 text-[#27AE60] border font-bold border-[#27AE60] rounded-md inline-block mt-4">مشاهده جزییات</button>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>

        </div>

    );
}
export default connect()(Question)
