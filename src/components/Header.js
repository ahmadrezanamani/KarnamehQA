import React, { Component } from 'react';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import { AddQuestion, GetAllQuestions } from "../store/actions/questions";
import {useSelector, useStore} from "react-redux";
import {hourTime, getDate} from '../utils'

export function Header({loading, setLoading}) {
    const store = useStore();
    const { id } = useParams();
    const [showModal, setShowModal] = React.useState(false);
    const [questionForm, setQuestionForm] = React.useState({
        "title": "",
        "question": "",
        "fullquestion": "",
    });
    const _AddQuestion = (body) => AddQuestion(body)(store.dispatch, store.getState);
    const _GetAllQuestions = args => GetAllQuestions(args)(store.dispatch, store.getState);
    const QuestionEvent = () => {
        _AddQuestion({
            "title": questionForm.title,
            "question": questionForm.question,
            "fullQuestion": questionForm.fullquestion,
            "created_on": {
                "date": getDate(),
                "hour": hourTime()
            }
        }).then(() => {
            setShowModal(false);
            setLoading(false);
            _GetAllQuestions().then(() => {
                setLoading(true);
            });
        });
    }
    return (
        <header className="mainHeader bg-white">
            <div className="flex items-center h-[68px] text-2xl font-extrabold px-[56px] py-4 justify-between">
                <h1>{id ? 'جزییات سوال' : 'لیست سوال'}</h1>
                <div className="leftside flex items-center">
                    <button 
                        className="bg-[#27AE60] flex items-center text-white py-2 px-5 rounded-md text-xs ml-11"
                        onClick={() => setShowModal(true)}
                    >
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg" className="ml-3">
                            <path fillRule="evenodd" clipRule="evenodd" d="M10 5C10.375 5 10.6793 5.30452 10.6793 5.67973V9.31954L14.3207 9.31972C14.6957 9.31972 15 9.62424 15 9.99946C15 10.3747 14.6957 10.6792 14.3207 10.6792L10.6793 10.679V14.3203C10.6793 14.6955 10.375 15 10 15C9.62504 15 9.32073 14.6955 9.32073 14.3203V10.679L5.67927 10.6792C5.30341 10.6792 5 10.3747 5 9.99946C5 9.62424 5.30341 9.31972 5.67927 9.31972L9.32073 9.31954V5.67973C9.32073 5.30452 9.62504 5 10 5Z" fill="white"/>
                        </svg>
                        سوال جدید
                    </button>
                    <div className="overflow-hidden ml-3 flex items-center">
                        <a href="#" className="text-sm text-[#454545] flex items-center">
                            <img src="https://dummyimage.com/64x64/000/fff" alt="" className="w-[44px] h-[44px] rounded-full ml-3 " />
                            <span className=" ml-4">الناز شاکردوست</span>
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10.8058 13.9033C10.4062 14.4472 9.59376 14.4472 9.19416 13.9033L5.10774 8.34214C4.62246 7.68172 5.09404 6.75 5.91358 6.75L14.0864 6.75C14.906 6.75 15.3775 7.68172 14.8923 8.34214L10.8058 13.9033Z" fill="#9CAEBB"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              <div className="border-0 rounded-lg shadow-2xl relative flex flex-col bg-white outline-none focus:outline-none w-[700px] overflow-hidden">
                <div className="flex items-start justify-between py-3 px-6 rounded-lg custom-shadow relative z-10">
                  <h3 className="text-base text-[#222222] font-extrabold">
                    ایجاد سوال جدید
                  </h3>
                  <button
                    className=""
                    onClick={() => setShowModal(false)}
                  >
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15 5L5 15" stroke="#454545" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                        <path d="M15 15L5 5" stroke="#454545" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                    </svg>
                  </button>
                </div>
                <div className="relative p-6 flex-auto bg-[#F9F9F9]">
                    <form action="">
                        <label htmlFor="title" className="mb-2.5 text-xs font-bold text-[#454545] block">موضوع</label>
                        <input 
                            type="text" 
                            placeholder="موضوع مورد نظر شما..." 
                            className="w-full py-3 px-4 border-[#EEEEEE] border rounded-lg" 
                            id="title" 
                            value={questionForm.title}
                            onChange={(e) => setQuestionForm({...questionForm, "title": e.target.value})}
                        />
                        <label htmlFor="content" className="mb-2.5 text-xs font-bold text-[#454545] block mt-4">متن سوال</label>
                        <textarea 
                            placeholder="متن مورد نظر شما" 
                            id="content"
                            className="w-full py-3 px-4 border-[#EEEEEE] border rounded-lg resize-none h-40"
                            value={questionForm.question}
                            onChange={(e) => setQuestionForm({...questionForm, "question": e.target.value, "fullquestion": e.target.value})}
                        />
                    </form>
                </div>
                <div className="flex items-center justify-end p-6 rounded-lg bg-[#F9F9F9]">
                  <button
                    className="text-[#27AE60] flex items-center py-2 px-5 ml-5 text-xs"
                    type="button"
                    onClick={() => setShowModal(false)}
                  >
                    انصراف
                  </button>
                  <button
                    className="bg-[#27AE60] flex items-center text-white py-2 px-5 rounded-lg text-xs leading-5"
                    type="button"
                    onClick={() => QuestionEvent()}
                  >
                    ایجاد سوال
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}

        </header>
    );
}
export default connect()(Header);
