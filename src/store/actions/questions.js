import axios from 'axios';

export function GetAllQuestions() {
    return (dispatch, getState) => {
        return new Promise(async (resolve, reject) => {
            await axios.get(`http://localhost:3000/questions?_embed=answers`).then(res => {
                if(res.data) {
                    dispatch({type: 'SET_ALL_QUESTIONS', payload: res.data});
                    resolve(res);
                }
            }).catch(reject);
        })
    }
}
export function GetQuestion(id) {
    return (dispatch, getState) => {
        return new Promise(async (resolve, reject) => {
            await axios.get(`http://localhost:3000/questions/${id}?_embed=answers`).then(res => {
                if(res.data) {
                    dispatch({type: 'SET_SINGLE_QUESTION', payload: res.data});
                    resolve(res);
                }
            }).catch(reject);
        })
    }
}
export function AddQuestion(body) {
    return (dispatch, getState) => {
        return new Promise(async (resolve, reject) => {
            await axios.post(`http://localhost:3000/questions`, body).then(res => {
                resolve(res);
            }).catch(reject);
        })
    }
}

export function AddAnswer(id, body) {
    return (dispatch, getState) => {
        return new Promise(async (resolve, reject) => {
            await axios.post(`http://localhost:3000/questions/${id}/answers`, body).then(res => {
                resolve(res);
            }).catch(reject);
        })
    }
}
export function LikeOrDislike(id, body) {
    return (dispatch, getState) => {
        return new Promise(async (resolve, reject) => {
            await axios.put(`http://localhost:3000/answers/${id}`, body).then(res => {
                resolve(res);
            }).catch(reject);
        })
    }
}
