import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import thunk from 'redux-thunk';
import rootReducer from './reducers'

export default function configureStore(initialState = {}) {
    return createStore(combineReducers(rootReducer), initialState, applyMiddleware(thunk))
}