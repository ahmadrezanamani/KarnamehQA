let initialState = {
  list: [],
  single: []
}

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case 'SET_ALL_QUESTIONS':
            if (action.payload) state.list = action.payload
            break
        case 'SET_SINGLE_QUESTION':
            if (action.payload) state.single = action.payload
            break
        default:
        return state
    }
    return state
}
