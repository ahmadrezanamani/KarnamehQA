import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';

import { Routes, Route } from 'react-router-dom';
  
import Question from './components/Question';
import SingleQuestion from './components/SingleQuestion';

class App extends Component {
    render() {
        return (
            <Routes>
                <Route path="/" element={<Question />}></Route>
                <Route path="/questions/:id" element={<SingleQuestion />}></Route>
            </Routes>
        );
    }
}
export default connect()(App);
