import React from 'react';
import { Provider } from 'react-redux'
import * as ReactDOMClient from 'react-dom/client';
import configureStore from './store';
import './index.css';
import App from './App';
import { BrowserRouter } from 'react-router-dom';

const root = ReactDOMClient.createRoot(document.getElementById('root'));

root.render(
    <Provider store={configureStore()}>
        <BrowserRouter>
            <App />
        </BrowserRouter>,
    </Provider>,
  );